## Fish Models

A lightweight reimplementation of Chanakya Ekbotes project, which can be found [here.](https://git.imp.fu-berlin.de/bioroboticslab/robofish/student_projects/fish-models/-/tree/master)
See there for documentation of the preprocessing functions and a literature survey. 

---


### Links/ References:


- Moritz Maxeiner's master thesis, based on which the project was developed, can be found [here.](https://www.mi.fu-berlin.de/inf/groups/ag-ki/Theses/Completed-theses/Master_Diploma-theses/2019/Maxeiner/MA-Maxeiner.pdf)
- The data used for the project can be found [here.](https://zenodo.org/record/3457834#.XtK4XcBS_IU)
- See [here](https://arxiv.org/pdf/1901.07859.pdf) for an explanation of Mixture Density RNNs. This architecture was implemented in addition to Mr. Ekbotes work. 

### Structure:

The repository contains:
- `python`: Scripts for data visualization, model training and evaluation 
- `models`: models trained with different architectures and corresponding evaluation plots and videos

The scripts are structured as follows:
- `training.py` is the main file for training the model
- `config.py` is the config file of the project and defines the hyper parameters for training, the network architectures used and the paths to the data. It is imported by all other files.
- `net_archs.py` defines different neural network architectures which can be trained and compared.
- `preprocessing.py` contains the functions for preprocessing the raw data to agent centric data as described in Mr. Maxeiners thesis.
- `reshaping.py` contains functions for reshaping the data according to the used network architecture. 
- `callbacks.py` contains the callback functions called in each training epoch.
- `evaluation.py` loads a trained model and lets that model generate trajectories of two fish which are then shown in an animation and evaluated according to two metrics.
- `visualization.py` contains a function, with which the raycasting function can be visualized, and hence the correctness of the preprocessing can be evaluated. 

### Usage:

- For **Training**, in `config.py` change the data path and model path, where the trained model should be saved, to your local path, choose the desired setup of hyper parameters. Then run the `training.py` script with python3.8. 
- For **Evaluation**, in `config.py` choose the path of the model to be loaded and the set of hyper parameters with that it was trained, then in `evaluation.py` choose starting positions for the two fish to be simulated, then run `evaluation.py` with python3.8. 





