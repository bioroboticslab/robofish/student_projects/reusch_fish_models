import h5py
import os
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib import rc, cm
from matplotlib.pyplot import subplots
from matplotlib.animation import FuncAnimation
from preprocessing import *
from numpy import float32
from numpy.linalg import norm
from net_archs import *
import config

plt.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'
plt.rcParams['animation.writer'] = '/usr/bin/ffmpeg'
print("Version:", tf.__version__)
Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)


"""### Prepocessing Functions"""
def import_data(path, random, no_of_trajectories, transform):
    if random == 1:
        random_selection = np.random.randint(0, np.shape(path)[0], no_of_trajectories)
    else:
        random_selection = np.array(range(len(path)))

    agents = []
    actual_no_of_agents = 0

    for j in random_selection:
        agents.append([])

    for j, p in zip(random_selection, range(len(random_selection))):

        hf = h5py.File(path[j], 'r')
        keys = list(hf.keys())

        for k in range(len(keys)):
            agents[p].append(np.asarray(hf.get(keys[k])))
            actual_no_of_agents += 1

        if transform == 1:
            agents[p] = transform_coordinates(np.asarray(agents[p]))
        else:
            agents[p] = np.asarray(agents[p])

    for j in range(len(agents) - 1):
        agents[0] = np.append(agents[0], agents[j + 1], axis=0)

    return agents[0]


"""### Visualization"""
class AgentAnimation:
    # Credits: Christopher Mühl, Patrick Winterstein

    def __init__(self, data, title):
        self.data = data
        self.num_agents = np.shape(data)[0]
        self.fig, self.ax = subplots(figsize=(8, 8))
        self.ax.set_xlim(-75, 75)
        self.ax.set_ylim(-75, 75)
        self.ax.set_xlabel('X Length (-75, 75)', fontsize=12)
        self.ax.set_ylabel('Y Length (-75, 75)', fontsize=12)
        self.fig.suptitle(title, fontsize=20)
        self.agents = []
        self.colors = cm.get_cmap('gist_rainbow')

    def create_agent(self, position, orientation, fish_id):
        return self.ax.plot(position[0], position[1], marker=(3, 0, orientation), linestyle='',
                            color=self.colors(fish_id / self.num_agents))[0]

    def create_frame(self, i):
        for agent in self.agents:
            agent.remove()
        self.agents = [self.create_agent(
            position=self.data[j, i, 0:2],
            orientation=180 / np.pi * np.arctan2(self.data[j, i, 3], self.data[j, i, 2]) - 90,
            fish_id=j,
        ) for j in range(self.num_agents)]
        return self.agents

    def animate(self, frames, interval):
        return FuncAnimation(self.fig, self.create_frame, frames=frames, interval=interval)


def animate(filepath, AgentAnimation, data, title, frames, interval, live=False):
    animation = AgentAnimation(data, title)
    video = animation.animate(frames=frames, interval=interval)
    video.save(filepath + ".mp4", writer=writer)
    plt.show()
    plt.close()

"""### Evaluation Functions"""

def params_to_sample(params):
    alpha, mu, sigma = [params[i * config.mixture_components:(i + 1) * config.mixture_components] for i in range(3)]
    gm = tfd.MixtureSameFamily(
        mixture_distribution=tfd.Categorical(probs=alpha),
        components_distribution=tfd.Normal(
            loc=mu,
            scale=sigma))
    return gm.sample()


def simulate_trajectory(fish_poses, no_of_simulated_agents, no_of_control_agents, control_agents_data, time_steps,
                        max_dist, wall_boundaries, no_of_bins_voa, no_of_bins_vow, total_angle_vow,
                        total_angle_voa, max_speed, no_speed_bins, max_ang_velo, ang_vel_bins):
    # model = models.load_model(path, custom_objects={'md_confidence': md_confidence, 'md_loss': md_loss, "mse_sample_loss": mse_sample_loss})
    # model = models.load_model(path, custom_objects={"my_top_k": my_top_k})
    if config.mixture_density:
        model1 = Ladder_Net_MD((1, 1, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.mixture_components, stateful=True)
        model2 = Ladder_Net_MD((1, 1, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.mixture_components, stateful=True)
    else:
        model1 = Ladder_Net((1, 1, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.speed_bins, config.ang_vel_bins, stateful=True)
        model2 = Ladder_Net((1, 1, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.speed_bins, config.ang_vel_bins, stateful=True)
    model1.build((1, 1, config.no_of_bins_voa + config.no_of_bins_vow + 2))
    model2.build((1, 1, config.no_of_bins_voa + config.no_of_bins_vow + 2))
    model1.load_weights(config.model_path + "weights.h5")
    model2.load_weights(config.model_path + "weights.h5")
    to_predict_data = np.zeros((no_of_simulated_agents, time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2))
    simulated_data = np.zeros((no_of_simulated_agents, time_steps + 1, 4))

    # to_predict_data = np.zeros((96, time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2))
    if no_of_control_agents > 0:
        control_agents_data = control_agents_data[:, 0: time_steps, :]
        control_agents_data = np.append(control_agents_data,
                                        np.zeros((no_of_control_agents, 1, np.shape(control_agents_data)[2])), axis=1)
        simulated_data = np.append(simulated_data, control_agents_data, axis=0)

    for i in range(no_of_simulated_agents):
        simulated_data[i, 0, :] = fish_poses[i]
        to_predict_data[i, 0, config.no_of_bins_voa + config.no_of_bins_vow: config.no_of_bins_voa + config.no_of_bins_vow + 1] = 0
        to_predict_data[i, 0, config.no_of_bins_voa + config.no_of_bins_vow + 1:] = 0.0

    for i in range(time_steps):
        for j in range(no_of_simulated_agents):
            if (no_of_simulated_agents + no_of_control_agents > 1):
                other_agents = np.append(simulated_data[0:j, i, :], simulated_data[j + 1:, i, :], axis=0)
                to_predict_data[j, i, 0: config.no_of_bins_voa] = return_view_of_the_agents(simulated_data[j, i, :],
                                                                                     other_agents, config.max_dist,
                                                                                     config.no_of_bins_voa, config.total_angle_voa)

            to_predict_data[j, i, config.no_of_bins_voa: config.no_of_bins_voa + config.no_of_bins_vow] = return_view_of_the_walls(
                simulated_data[j, i, :], config.wall_boundaries, config.max_dist, config.no_of_bins_vow, config.total_angle_vow)

            prediction_data = np.expand_dims(np.asarray([to_predict_data[j, i, :]]), axis=0)
            if config.arch == 1:
                shape_pd = prediction_data.shape
                prediction_data = prediction_data.reshape((shape_pd[0], shape_pd[1], 1, 2, config.no_of_bins_voa + 1))
            if j == 0:
                speed, ang_vel = model1.predict(prediction_data)
            else:
                speed, ang_vel = model2.predict(prediction_data)

            # speed = speed[0, i, :]
            # ang_vel = ang_vel[0, i, :]
            if config.mixture_density:
                speed = params_to_sample(speed[0, 0, :])
                ang_vel = params_to_sample(ang_vel[0, 0, :])
            else:
                speed, ang_vel = return_binned_to_speeds(speed, ang_vel, config.max_speed, no_speed_bins, config.max_ang_velo, config.ang_vel_bins)
            # update sensory input and position for next timestep
            if i < time_steps - 1:
                to_predict_data[j, i + 1, config.no_of_bins_voa + config.no_of_bins_vow: config.no_of_bins_voa + config.no_of_bins_vow + 1] = speed
                to_predict_data[j, i + 1, config.no_of_bins_voa + config.no_of_bins_vow + 1:] = ang_vel
            simulated_data[j, i + 1, :] = velocity_to_current_position(speed, ang_vel, simulated_data[j, i, :])

    return simulated_data, prediction_data, to_predict_data


# from Moritz Maxeiner
def normalize_series(x):
    """
    Given a series of vectors, return a series of normalized vectors.
    Null vectors are mapped to `NaN` vectors.
    """
    return (x.T / norm(x, axis=-1)).T


def calc_iid(a, b):
    """
    Given two series of poses - with X and Y coordinates of their positions as the first two elements -
    return the inter-individual distance (between the positions).
    """
    return norm(b[:, :2] - a[:, :2], axis=-1)


def calc_tlvc(a, b, tau_min, tau_max):
    """
    Given two velocity series and both minimum and maximum time lag return the
    time lagged velocity correlation from the first to the second series.
    """
    length = tau_max - tau_min
    return float32(
        [
            (a[t] @ b[t + tau_min:][:length].T).mean()
            for t in range(min(len(a), len(b) - tau_max + 1))
        ]
    )


def calc_follow(a, b):
    """
    Given two series of poses - with X and Y coordinates of their positions as the first two elements -
    return the follow metric from the first to the second series.
    """
    a_v = a[1:, :2] - a[:-1, :2]
    b_p = normalize_series(b[:-1, :2] - a[:-1, :2])
    return (a_v * b_p).sum(axis=-1)

def experiments(fish_poses, tags):
    for i in range(len(fish_poses)):
        path = config.model_path + tags[i] + "_simulation"
        print(path)
        if not os.path.isfile(path + ".npy"):
            simulation, reshape, to_predict_data = simulate_trajectory(fish_poses[i], no_of_simulated_agents,
                                                                       no_of_control_agents,
                                                                       control_agents_data, time_steps, config.max_dist,
                                                                       config.wall_boundaries, config.no_of_bins_voa, config.no_of_bins_vow,
                                                                       config.total_angle_vow, config.total_angle_voa, config.max_speed,
                                                                       config.speed_bins, config.max_ang_velo, config.ang_vel_bins)
            np.save(path + ".npy", simulation)
        else:
            simulation = np.load(path + ".npy")

        # plot a trajectory
        x = simulation[0, 0:750, 0]
        y = simulation[0, 0:750, 1]
        plt.plot(x, y)
        x = simulation[1, 0:750, 0]
        y = simulation[1, 0:750, 1]
        plt.plot(x, y)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("trajectory" + tags[i])
        plt.show()

        if not os.path.isfile(path + ".mp4"):
            animate(path, AgentAnimation, simulation, 'Simulation Model and Live Fish 2', time_steps, 75)

        follow = calc_follow(simulation[0, :, :], simulation[1, :, :])
        iid = calc_iid(simulation[0, :, :], simulation[1, :, :])
        plt.scatter(iid[:-1], follow)
        plt.xlabel('IID')
        plt.ylabel('Velocity Correlation')
        plt.title('Follow Simulated Fish ' + tags[i])
        plt.savefig(path + "_follow.png")
        plt.show()


        mean = follow.mean()
        plt.plot(np.arange(0, time_steps ), follow)
        plt.xlabel('time')
        plt.ylabel('Velocity Correlation')
        plt.title('Follow Simulated Fish ' + tags[i] + " mean: " + f'{mean:5.2f}')
        plt.savefig(path + "_follow-time.png")
        plt.show()

        tlvc = calc_tlvc(simulation[0, :, :], simulation[1, :, :], 8, 30)
        # iid = calc_iid(simulation[0, :, :], simulation[1, :, :])
        plt.scatter(iid[:len(tlvc)], tlvc)
        plt.xlabel('IID')
        plt.ylabel('TLVC')
        plt.title('TLVC Simulated Fish ' + tags[i])
        plt.savefig(path + "_tlvc.png")
        plt.show()

        mean = tlvc.mean()
        plt.plot(np.arange(0, len(tlvc) ), tlvc)
        plt.xlabel('time')
        plt.ylabel('Velocity Correlation')
        plt.title('TLVC Simulated Fish ' + tags[i] + " mean: " + f'{mean:5.2f}')
        plt.savefig(path + "_tlvc-time.png")
        plt.show()

"""# Model Evaluation
### Data Import and trajectory creation
"""
if __name__ == "__main__":
    data_2_fish = import_data(config.train_path, 1, 1, 1)
    no_of_control_agents = 0
    no_of_simulated_agents = 2
    time_steps = 1500
    limit = 75
    control_agents_data = data_2_fish[1:]


    """### Visualization
    """
    #### Actual Fish
    # animate(AgentAnimation, data_2_fish, 'Live Female Female 2', time_steps, 75, live=True)
    follow = calc_follow(data_2_fish[0, :, :], data_2_fish[1, :, :])
    iid = calc_iid(data_2_fish[0, :, :], data_2_fish[1, :, :])
    plt.scatter(iid[:-1], follow)
    plt.xlabel('IID')
    plt.ylabel('Velocity Correlation')
    plt.title('Follow Real Fish')
    plt.show()

    tlvc = calc_tlvc(data_2_fish[0, :, :], data_2_fish[1, :, :], 8, 30)
    plt.scatter(iid[:len(tlvc)], tlvc)
    plt.xlabel('IID')
    plt.ylabel('TLVC')
    plt.title('TLVC Real Fish')
    plt.show()

    # plot a trajectory
    x = data_2_fish[0, 0:750, 0]
    y = data_2_fish[0, 0:750, 1]
    plt.plot(x, y)
    x = data_2_fish[1, 0:750, 0]
    y = data_2_fish[1, 0:750, 1]
    plt.plot(x, y)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("trajectory live" )
    plt.show()

    fish_poses = np.asarray([[[3, 3, 0.5, 0.5], [0, 0, -0.5, -0.5]],
                             [[3, 3, -0.5, -0.5], [0, 0, 0.5, 0.5]],
                             [[3, 3, 0.5, 0.5], [0, 0, 0.5, 0.5]],
                             [[40, 40, 0.5, 0.5], [-40, -40, -0.5, -0.5]],
                             [[40, 40, -0.5, -0.5], [-40, -40, 0.5, 0.5]],
                             [[40, 40, 0.5, 0.5], [-40, -40, 0.5, 0.5]],
                             [[20, 0, 1, 0], [-20, 0, -1, 0]],
                             [[20, 0, -1, 0], [-20, 0, 1, 0]],
                             [[20, 0, 1, 0], [-20, 0, 1, 0]]])
    tags = ["near-opposite", "near-towards", "near-same",
            "far-opposite", "far-towards", "far-same",
            "mid-opposite", "mid-towards", "mid-same"]

    experiments(fish_poses, tags)

