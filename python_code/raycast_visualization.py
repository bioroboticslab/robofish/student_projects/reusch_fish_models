import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches
from descartes.patch import PolygonPatch
import shapely
from shapely.geometry import Polygon as ShapelyPolygon
from reshaping import print_shapes
import sys
from config import *
from preprocessing import data_to_model

GRAY = '#999999'
DARKGRAY = '#333333'
YELLOW = '#ffcc33'
GREEN = '#339933'
RED = '#ff3333'
BLACK = '#000000'
BLUE = '#6699cc'

def set_limits(ax, x0, xN, y0, yN):
    ax.set_xlim(x0, xN)
    ax.set_xticks(range(x0, xN + 1, 10))
    ax.set_ylim(y0, yN)
    ax.set_yticks(range(y0, yN + 1, 10))
    ax.set_aspect("equal")


def plot_coords(ax, ob, color=GRAY, zorder=1, alpha=1):
    x, y = ob.xy
    ax.plot(x, y, 'o', color=color, zorder=zorder, alpha=alpha)


def print_fish(ax, pose):
    ori = (np.arctan2(pose[3], pose[2]) - np.pi / 2) / np.pi * 180

    ellipse = patches.Ellipse((pose[0], pose[1]), 2, 7, ori, color=RED)
    ax.add_patch(ellipse)




def plot_intensities(X):
    for i in range(X.shape[0]):
        X[i] = np.min(X[i])
    print(X.shape)
    plt.plot(X)
    plt.show()


# ---------- Visualization of the precomputed raycast data ----------------

tank_walls = [((1, 0), (0, 0)), ((1, 0), (0, 100)), ((0, 1), (0, 0)), ((0, 1), (100, 0))]


def is_in_tank(x, y):
    return 0 <= x <= 100 and 0 <= y <= 100


from math import pi, cos, sin


def ray_intersection(x, a):
    """computes intersection of a ray emitted from pos x with angle a with the tank walls"""
    # we have the following equations:
    # lambda * cos(a) + x1 = mu * w1 + b1
    # lambda * sin(a) + x2 = mu * w2 + b2
    # x = (x.x, x.y)
    x = list(x)

    # make sure positions are within tank walls
    x[0] = 0 if x[0] < 0 else x[0]
    x[1] = 0 if x[1] < 0 else x[1]
    x[0] = 100 if x[0] > 100 else x[0]
    x[1] = 100 if x[1] > 100 else x[1]

    a = a % (2 * pi)
    c = cos(a)
    s = sin(a)
    # if we have a case where cos or sin are close to 0, we can immediately return some value
    if abs(c) < 0.0001:
        # print('edge case: angle almost 90 or 270')
        if a > 0:
            return x[0], 100
        else:
            return x[0], 0
    elif abs(s) < 0.0001:
        if abs(a) < 0.9:
            return 100, x[1]
        else:
            return 0, x[1]

    # else we iterate through the lines which represent the tank walls
    # and search the intersection point with the line given by position and orientation-angle of the agent
    # there has to be exactly one intersection point within the tank walls in the direction of the ray
    for w, b in tank_walls:
        lambd = 0
        if w[0] == 0:
            lambd = (b[0] - x[0]) / c
        elif w[1] == 0:
            lambd = (b[1] - x[1]) / s

        # we want the intersection in the direction of the ray, so lambda has to be bigger than 0
        if lambd >= 0:
            inters = round(lambd * c + x[0], 6), round(lambd * s + x[1], 6)
            if is_in_tank(inters[0], inters[1]):
                return inters
    print("ERROR: no intersection point found!")
    print("Position: ", x[0], x[1])
    print("Orientation: ", a)
    print("cos(a): ", c, " sin(a): ) ", s)
    return -1, -1

def addCorner(p1, p2):
    x = p2[0] if 0 < p1[0] < 100 else p1[0]
    y = p2[1] if 0 < p1[1] < 100 else p1[1]
    return x, y


def plot_pre_comp_data(X, num_guppy_bins, raw_data, index, stateful):
    agent = X[index]
    agent_raw = raw_data[index]
    other_raw = raw_data[index + 1]

    # transform back into coord system with 0,0 bottom left
    agent_raw[:, 0] = agent_raw[:, 0] + 50
    agent_raw[:, 1] = - (agent_raw[:, 1] - 50)
    agent_raw[:, 3] = - agent_raw[:, 3]
    other_raw[:, 0] = other_raw[:, 0] + 50
    other_raw[:, 1] = - (other_raw[:, 1] - 50)
    other_raw[:, 3] = - other_raw[:, 3]

    agent_pos = agent_raw[:, :2]
    agent_ori = (np.arctan2(agent_raw[:, 3], agent_raw[:, 2]) - np.pi / 2)

    bin_angles = [total_angle_voa * (i / num_guppy_bins) - pi / 2 + (2 * pi - total_angle_voa) / 2
                  for i in range(0, num_guppy_bins + 1)]

    fig, ax = plt.subplots()
    set_limits(ax, 0, 100, 0, 100)

    if not stateful:
        for t in range(agent.shape[0]):
            ax.cla()
            set_limits(ax, 0, 100, 0, 100)
            intersections = [ray_intersection(agent_pos[t], angle + agent_ori[t]) for angle in bin_angles]
            # construct the bins as polygons with intersection points and observer position as vertices
            bins = []
            for i in range(len(intersections) - 1):
                if intersections[i][0] == intersections[i + 1][0] or intersections[i][1] == intersections[i + 1][1]:
                    bins.append(ShapelyPolygon([agent_pos[t], intersections[i], intersections[i + 1]]))
                else:  # if the intersection points overlap a corner of the tank, we have to add that corner to the polygon
                    corner = addCorner(intersections[i], intersections[i + 1])
                    bins.append(ShapelyPolygon([agent_pos[t], intersections[i], corner, intersections[i + 1]]))

            for i, p in enumerate(bins):
                patch = PolygonPatch(p, facecolor=BLACK, edgecolor=YELLOW,
                                     alpha=(1 - agent[t, i]), zorder=1)
                ax.add_patch(patch)

            print_fish(ax, agent_raw[t])
            print_fish(ax, other_raw[t])
            # legend
            ax.text(110, 100, 'angle: {:.2f}°'.format(agent_ori[t]))
            ax.text(110, 90, 'time: {}'.format(t))
            # ax.text(110, 90, 'o_vector: ({:.2f},{:.2f})'.format(agent_ori[t, 0], agent_ori[t, 1]))
            # ax.text(110, 80, 'angular turn: {:.10f}'.format(loc_vec[0]))
            # ax.text(110, 70, 'linear speed: {:.10f}'.format(loc_vec[1]))
            # ax.text(110, 60, 'dist travelled: {:.10f}'.format(dist_difference))
            plt.show(block=False)
            plt.pause(0.01)
    else:
        # the simulation shows that the partitioning of the data for keras stateful prediction is correct,
        # because every 96th batch you have 150 timesteps for one agent
        step = 96
        k = 0
        for j in range(index, X.shape[0], step):
            agent = X[j]
            for t in range(agent.shape[0]):
                ax.cla()
                set_limits(ax, 0, 100, 0, 100)
                intersections = [ray_intersection(agent_pos[k], angle + agent_ori[k]) for angle in bin_angles]
                # construct the bins as polygons with intersection points and observer position as vertices
                bins = []
                for i in range(len(intersections) - 1):
                    if intersections[i][0] == intersections[i + 1][0] or intersections[i][1] == intersections[i + 1][1]:
                        bins.append(ShapelyPolygon([agent_pos[k], intersections[i], intersections[i + 1]]))
                    else:  # if the intersection points overlap a corner of the tank, we have to add that corner to the polygon
                        corner = addCorner(intersections[i], intersections[i + 1])
                        bins.append(ShapelyPolygon([agent_pos[k], intersections[i], corner, intersections[i + 1]]))

                for i, p in enumerate(bins):
                    patch = PolygonPatch(p, facecolor=BLACK, edgecolor=YELLOW,
                                         alpha=(1 - agent[t, i]), zorder=1)
                    ax.add_patch(patch)

                k += 1
                print_fish(ax, agent_raw[k])
                print_fish(ax, other_raw[k])
                # legend
                ax.text(110, 100, 'angle: {:.2f}°'.format(agent_ori[k]))
                ax.text(110, 90, 'timestep: {}'.format(k))
                plt.show(block=False)
                plt.pause(0.01)


X_train, Y1_train, Y2_train, raw_data_train = data_to_model(train_path, 0, max_dist,
                                                            no_of_bins_voa, total_angle_voa, wall_boundaries, no_of_bins_vow,
                                                            total_angle_vow, max_speed, speed_bins, max_ang_velo, ang_vel_bins,
                                                            mixture_density=mixture_density, recompute_labels=False)
plot_pre_comp_data(X_train, no_of_bins_voa, raw_data_train, 10, stateful)
