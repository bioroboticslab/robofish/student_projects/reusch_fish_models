import numpy as np
import math
import h5py
import multiprocessing
from joblib import Parallel, delayed
import os

def transform_coordinates(state):
    # state has x , y, dx ,dy
    state_ = np.zeros_like(state)
    state_[:, :, 0] = state[:, :, 0] - 50
    state_[:, :, 1] = - state[:, :, 1] + 50
    state_[:, :, 2] = state[:, :, 2]
    state_[:, :, 3] = - state[:, :, 3]
    return state_


def return_view_of_the_agents(socf, soof, max_dist, no_of_bins, total_angle):
    # socf: state of fish under considerdation
    # soof: states of other fish (can be a batch)
    # total angle is in radian (angle whos bins are to be taken)
    # The logic for this method is that we look at at how close the other fish are to the the angle biscetors using a dot product.
    orientation_socf = np.arctan2(socf[3], socf[2])
    angle_bisectors = np.array(range(no_of_bins // 2, -(no_of_bins // 2) - 1 , -1)) * total_angle / no_of_bins + orientation_socf
    shape = np.shape(soof)
    socf_vector_cos = np.cos(angle_bisectors)
    socf_vector_sin = np.sin(angle_bisectors)
    view_of_the_agents = np.zeros(no_of_bins)
    for i in range(shape[0]):
        soof_X = soof[i, 0] - socf[0]
        soof_Y = soof[i,1] - socf[1]
        soof_vector_cos = np.cos(np.arctan2(soof_Y, soof_X))
        soof_vector_sin = np.sin(np.arctan2(soof_Y, soof_X))
        angle = np.arccos(np.clip(soof_vector_cos * socf_vector_cos[no_of_bins // 2] + soof_vector_sin * socf_vector_sin[no_of_bins // 2], -1, 1))
        if angle > - total_angle/2 and angle < total_angle/2:
            max_bin = np.argmax(socf_vector_cos * soof_vector_cos + socf_vector_sin * soof_vector_sin)
            distance = np.sqrt(soof_X ** 2 + soof_Y ** 2)
            if view_of_the_agents[max_bin] < 1 - distance / max_dist :
                view_of_the_agents[max_bin] = 1 - distance / max_dist
    return view_of_the_agents


def return_view_of_the_walls(socf, wall_boundaries, max_dist, no_of_bins, total_angle):
    # wall_boundaries contain +x, -x, +y and -y of the walls (Basically equations of the lines that describe the walls)
    orientation_socf = np.arctan2(socf[3], socf[2])
    angles = np.array(range(no_of_bins // 2, -(no_of_bins // 2) - 1 , -1)) * total_angle / no_of_bins + orientation_socf
    slopes = np.tan(angles)
    socf_cos = np.cos(angles)
    socf_sin = np.sin(angles)
    #     print(slopes)
    #     print(angles)
    view_of_walls = np.zeros(no_of_bins)
    for i in range(len(slopes)):
        for j in range(len(wall_boundaries)):
            if (j < 2):
                if (slopes[i] > np.tan(0.99 * np.pi/2)) or (slopes[i] < np.tan(-0.99 * np.pi/2)) :
                    # Condition to not evaluate the lines paralell to lines +x and - x
                    continue
                else:
                    y = slopes[i] * (wall_boundaries[j]*1.0 - socf[0]) + socf[1]
                    angle = np.arctan2((y - socf[1]), (wall_boundaries[j]*1.0 - socf[0]))
                    vector_cos = np.cos(angle)
                    vector_sin = np.sin(angle)
                    index = np.argmax(vector_cos * socf_cos + vector_sin * socf_sin)
                    #                     print(i, index, wall_boundaries[j], y)
                    if (y < wall_boundaries[2]) and (y > wall_boundaries[3]) and (index == i):
                        #                         print('Selected:', y)
                        view_of_walls[i] = 1 - np.sqrt((socf[0] - wall_boundaries[j])**2 + (socf[1] - y)**2) / max_dist
                        break
            if (j>=2 and j < 4):
                if slopes[i] < np.tan(0.001 * np.pi/2) and slopes[i] > np.tan(-0.001 * np.pi/2) :
                    # Condition to not evaluate the lines parallel or almost parallel to + y and - y
                    continue
                else:
                    x = (wall_boundaries[j] - socf[1]) / slopes[i] + socf[0]
                    angle = np.arctan2((wall_boundaries[j] - socf[1]), (x - socf[0]))
                    vector_cos = np.cos(angle)
                    vector_sin = np.sin(angle)
                    index = np.argmax(vector_cos * socf_cos + vector_sin * socf_sin)
                    #                     print(i, index, x, wall_boundaries[j])
                    if  (x < wall_boundaries[0]) and (x > wall_boundaries[1]) and (index == i):
                        #                         print('Selected', x)
                        view_of_walls[i] = 1 - np.sqrt((socf[0] - x)**2 + (socf[1] - wall_boundaries[j])**2) / max_dist
                        break

    return view_of_walls


def return_binned_velocities(current_state, previous_state, max_linear_velo, lin_vel_bins, max_ang_velo, ang_vel_bins):
    vel_lin_bins = np.array(range(0, lin_vel_bins + 1 , 1)) * max_linear_velo / lin_vel_bins - max_linear_velo / 2
    vel_ang_bins = np.array(range(0, ang_vel_bins + 1 , 1)) * max_ang_velo / ang_vel_bins - max_ang_velo / 2

    vel_x = current_state[0] - previous_state[0]
    vel_y = current_state[1] - previous_state[1]
    vel_ang = np.arctan2(current_state[3], current_state[2]) - np.arctan2(previous_state[3], previous_state[2])
    vel_x_binned = np.zeros(lin_vel_bins)
    vel_y_binned = np.zeros(lin_vel_bins)
    vel_ang_binned = np.zeros(ang_vel_bins)

    for i in range(lin_vel_bins):
        if vel_x > vel_lin_bins[i] and vel_x < vel_lin_bins[i + 1]:
            vel_x_binned[i] = 1
        if vel_y > vel_lin_bins[i] and vel_y < vel_lin_bins[i + 1]:
            vel_y_binned[i] = 1

        for i in range(ang_vel_bins):
            if vel_ang > vel_ang_bins[i] and vel_ang < vel_ang_bins[i + 1]:
                vel_ang_binned[i] = 1

    return vel_x_binned, vel_y_binned, vel_ang_binned


def return_speeds(current_state, previous_state, max_speed, max_ang_velo):
    speed = np.sqrt((current_state[0] - previous_state[0])**2 + (current_state[1] - previous_state[1])**2)
    speed = min(speed, max_speed)
    angular_velocity = np.arctan2(current_state[3], current_state[2]) - np.arctan2(previous_state[3], previous_state[2])
    if angular_velocity > np.pi:
        angular_velocity = angular_velocity - 2 * np.pi
    if angular_velocity < -np.pi:
        angular_velocity = angular_velocity + 2 * np.pi

    angular_velocity = min(angular_velocity, max_ang_velo) * (angular_velocity >= 0) + max(angular_velocity, -max_ang_velo) * (angular_velocity < 0)
    return speed, angular_velocity


def return_binned_speeds(current_state, previous_state, max_speed, no_speed_bins, max_ang_velo, ang_vel_bins):
    speed = np.sqrt((current_state[0] - previous_state[0])**2 + (current_state[1] - previous_state[1])**2)
    speed = min(speed, max_speed)
    speed_bins = np.array(range(-no_speed_bins, no_speed_bins + 1 , 2)) * max_speed / (no_speed_bins - 1)
    vel_ang_bins = np.array(range(-ang_vel_bins, ang_vel_bins + 1, 2)) * max_ang_velo / (ang_vel_bins - 1)
    vel_ang = np.arctan2(current_state[3], current_state[2]) - np.arctan2(previous_state[3], previous_state[2])

    if vel_ang > np.pi:
        vel_ang = vel_ang - 2 * np.pi
    if vel_ang < -np.pi:
        vel_ang = vel_ang + 2 * np.pi

    vel_ang = min(vel_ang, max_ang_velo) * (vel_ang >= 0) + max(vel_ang, -max_ang_velo) * (vel_ang < 0)
    vel_ang_binned = np.zeros(ang_vel_bins)
    speed_binned = np.zeros(no_speed_bins)

    #     print(speed_bins)
    #     print(vel_ang_bins)
    for i in range(no_speed_bins):
        if speed > speed_bins[i] and speed < speed_bins[i + 1]:
            speed_binned[i] = 1

    for i in range(ang_vel_bins):
        if vel_ang > vel_ang_bins[i] and vel_ang < vel_ang_bins[i + 1]:
            vel_ang_binned[i] = 1

    return speed_binned, vel_ang_binned


def data_to_model(path, random, max_dist, no_of_bins_voa, total_angle_voa, wall_boundaries,
                  no_of_bins_vow, total_angle_vow, max_speed, speed_bins, max_ang_velo, ang_vel_bins, mixture_density=False,
                  recompute_labels=False):
    """transforms raw data into handcrafted input vectors + labels to feed into network"""

    #load data from disk if possible
    if os.path.isfile(path[0] + "X_preprocessed.npy") and not recompute_labels:
        print("returning")
        return np.load(path[0] + "X_preprocessed.npy"), \
               np.load(path[0] + "Y1_preprocessed.npy"), \
               np.load(path[0] + "Y2_preprocessed.npy"), \
               np.load(path[0] + "raw_data.npy")


    agents = []
    actual_no_of_agents = 0
    # load the raw data for different agents into agents array
    for j in range(len(path)):
        agents.append([])
        hf = h5py.File(path[j], 'r')
        keys = list(hf.keys())

        for k in range(len(keys)):
            agents[j].append(np.asarray(hf.get(keys[k])))
            actual_no_of_agents += 1

        agents[j] = transform_coordinates(np.asarray(agents[j]))

    agents_voa_vow_sp_av = []
    agents_sp_binned = []
    agents_va_binned = []

    for j in range(len(agents)):
        print("initializing data for agent ", j)
        shape = np.shape(agents[j])
        agents_voa_vow_sp_av.append(np.zeros((shape[0], shape[1], no_of_bins_voa + no_of_bins_vow + 2)))
        if mixture_density: # mixture density has only one value as label for speed and angle
            agents_sp_binned.append(np.zeros((shape[0], shape[1] - 1, 1)))
            agents_va_binned.append(np.zeros((shape[0], shape[1] - 1, 1)))
        else:
            agents_sp_binned.append(np.zeros((shape[0], shape[1] - 1, speed_bins)))
            agents_va_binned.append(np.zeros((shape[0], shape[1] - 1, ang_vel_bins)))

    def compute_agent_data(j, agents, agents_voa_vow_sp_av, agents_sp_binned, agents_va_binned):
        """preprocess data for agent j"""
        print("computing data for agent ", j)
        shape = np.shape(agents)
        # shape 0: number of agents
        # shape 1: timesteps
        for k in range(shape[1]):
            for l in range(shape[0]):
                if not recompute_labels:
                    if shape[0] > 0:
                        other_agents = np.append(agents[0:l, k, :], agents[l + 1:, k, :], axis=0)
                        agents_voa_vow_sp_av[l, k, 0:no_of_bins_voa] = return_view_of_the_agents(agents[l, k, :],
                                                                                                 other_agents, max_dist,
                                                                                                 no_of_bins_voa,
                                                                                                 total_angle_voa)

                    agents_voa_vow_sp_av[l, k, no_of_bins_voa: no_of_bins_voa + no_of_bins_vow] = return_view_of_the_walls(
                        agents[l, k, :], wall_boundaries, max_dist, no_of_bins_vow, total_angle_vow)

                if k > 0:
                    speed, ang = return_speeds(agents[l, k, :], agents[l, k - 1, :], max_speed, max_ang_velo)
                    agents_voa_vow_sp_av[l, k, no_of_bins_voa + no_of_bins_vow] = speed
                    agents_voa_vow_sp_av[l, k, no_of_bins_voa + no_of_bins_vow + 1] = ang

                    if mixture_density: #the mixture density model needs raw scores for speed and angle as labels
                        agents_sp_binned[l, k - 1, :], agents_va_binned[l, k - 1, :] = speed, ang
                    else: #multimodal model needs speed and angle discretized into bins
                        agents_sp_binned[l, k - 1, :], agents_va_binned[l, k - 1, :] = return_binned_speeds(agents[l, k, :], agents[l, k - 1,:], max_speed, speed_bins, max_ang_velo, ang_vel_bins)
        return agents_voa_vow_sp_av, agents_sp_binned, agents_va_binned

    # we can do the preprocessing for the agents in parallel, by passing a function to the Parallel class
    # which does the preprocessing for agent j
    num_cores = multiprocessing.cpu_count()
    r = Parallel(n_jobs=num_cores, max_nbytes="50M")(
        delayed(compute_agent_data)(j, agents[j], agents_voa_vow_sp_av[j], agents_sp_binned[j], agents_va_binned[j])
        for j in range(len(agents))
    )

    #collect the results from parallelization
    for j, res in enumerate(r):
        agents_voa_vow_sp_av[j] = res[0]
        agents_sp_binned[j] = res[1]
        agents_va_binned[j] = res[2]

    # stack up results in a single numpy array
    for j in range(len(agents) - 1):
        agents_voa_vow_sp_av[0] = np.append(agents_voa_vow_sp_av[0], agents_voa_vow_sp_av[j + 1], axis=0)
        agents_sp_binned[0] = np.append(agents_sp_binned[0], agents_sp_binned[j + 1], axis=0)
        agents_va_binned[0] = np.append(agents_va_binned[0], agents_va_binned[j + 1], axis=0)
        agents[0] = np.append(agents[0], agents[j + 1], axis=0)

    X = agents_voa_vow_sp_av[0][:, 0:np.shape(agents_voa_vow_sp_av[0])[1] - 1]
    Y1 = agents_sp_binned[0]
    Y2 = agents_va_binned[0]
    raw_data = agents[0]

    if not recompute_labels: # save the precomputed data to disk
        np.save(path[0] + "X_preprocessed", X)
        np.save(path[0] + "Y1_preprocessed", Y1)
        np.save(path[0] + "Y2_preprocessed", Y2)
        np.save(path[0] + "raw_data", raw_data)
    else:
        X = np.load(path[0] + "X_preprocessed.npy")
        np.save(path[0] + "Y1_preprocessed", Y1)
        np.save(path[0] + "Y2_preprocessed", Y2)

    return X, Y1, Y2, raw_data


def return_binned_to_speeds(speed_binned, angular_velocity_binned, max_speed, no_speed_bins, max_ang_velo,
                            ang_vel_bins):
    speed_bins = np.array(range(-no_speed_bins, no_speed_bins + 1, 2)) * max_speed / (no_speed_bins - 1)
    vel_ang_bins = np.array(range(-ang_vel_bins, ang_vel_bins + 1, 2)) * max_ang_velo / (ang_vel_bins - 1)
    speed_binned = speed_binned.reshape(-1)
    angular_velocity_binned = angular_velocity_binned.reshape(-1)
    speed_bin_indices = np.asarray(range(0, np.shape(speed_binned)[0]))
    vel_bin_indices = np.asarray(range(0, np.shape(angular_velocity_binned)[0]))
    speed_bin_choice = np.random.choice(speed_bin_indices, p=speed_binned)
    vel_bin_choice = np.random.choice(vel_bin_indices, p=angular_velocity_binned)
    speed = np.random.uniform(speed_bins[0:np.shape(speed_binned)[0]][speed_bin_choice],
                              speed_bins[1:(np.shape(speed_binned)[0] + 1)][speed_bin_choice])
    ang_vel = np.random.uniform(vel_ang_bins[0:np.shape(angular_velocity_binned)[0]][vel_bin_choice],
                                vel_ang_bins[1:(np.shape(angular_velocity_binned)[0] + 1)][vel_bin_choice])
    #     speed_index = np.argmax(speed_binned)
    #     vel_index = np.argmax(angular_velocity_binned)
    #     speed = (speed_bins[0:np.shape(speed_binned)[0]][speed_index] + speed_bins[1:(np.shape(speed_binned)[0] + 1)][speed_index])/2
    #     ang_vel = (vel_ang_bins[0:np.shape(angular_velocity_binned)[0]][vel_index] + vel_ang_bins[1:(np.shape(angular_velocity_binned)[0] + 1)][vel_index])/2
    return speed, ang_vel


def velocity_to_current_position(speed, ang_vel, prev_position):
    current_position = np.zeros(np.shape(prev_position))
    current_position[3] = np.sin(ang_vel + np.arctan2(prev_position[3], prev_position[2]))
    current_position[2] = np.cos(ang_vel + np.arctan2(prev_position[3], prev_position[2]))
    current_position[1] = prev_position[1] + speed * current_position[3]
    current_position[0] = prev_position[0] + speed * current_position[2]
    return current_position


def return_speeds(current_state, previous_state, max_speed, max_ang_velo):
    speed = np.sqrt((current_state[0] - previous_state[0]) ** 2 + (current_state[1] - previous_state[1]) ** 2)
    speed = min(speed, max_speed)
    angular_velocity = np.arctan2(current_state[3], current_state[2]) - np.arctan2(previous_state[3], previous_state[2])
    if angular_velocity > np.pi:
        angular_velocity = angular_velocity - 2 * np.pi
    if angular_velocity < -np.pi:
        angular_velocity = angular_velocity + 2 * np.pi
    angular_velocity = min(angular_velocity, max_ang_velo) * (angular_velocity >= 0) + max(angular_velocity,
                                                                                           -max_ang_velo) * (
                               angular_velocity < 0)
    return speed, angular_velocity

