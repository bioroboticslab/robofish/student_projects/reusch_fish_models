from net_archs import *
from preprocessing import *
from reshaping import *
from callbacks import ResetHidden, SaveModel, tensorboard_callback
import config
import tensorflow as tf

# enable memory growth for gpu usage
gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)
print("Version:", tf.__version__)

"""### Training"""
# to change the paths to data see config.py
#-------------------load the data---------------------------
X_train, Y1_train, Y2_train, raw_data_train = data_to_model(config.train_path, 0, config.max_dist,
                                                            config.no_of_bins_voa, config.total_angle_voa,
                                                            config.wall_boundaries, config.no_of_bins_vow,
                                                            config.total_angle_vow, config.max_speed,
                                                            config.speed_bins, config.max_ang_velo,
                                                            config.ang_vel_bins, mixture_density=config.mixture_density,
                                                            recompute_labels=False)
X_val, Y1_val, Y2_val, raw_data_val = data_to_model(config.validation_path, 0, config.max_dist,
                                                    config.no_of_bins_voa, config.total_angle_voa,
                                                    config.wall_boundaries, config.no_of_bins_vow,
                                                    config.total_angle_vow, config.max_speed,
                                                    config.speed_bins, config.max_ang_velo,
                                                    config.ang_vel_bins, mixture_density=config.mixture_density,
                                                    recompute_labels=False)
print("data loaded")
print_shapes((X_train, Y1_train, Y2_train, X_val, Y1_val, Y2_val),
             ("X_train", "Y1_train", "Y2_train", "X_val", "Y1_val", "Y2_val"))

# ------------------------Reshape Data--------------------------------------
num_agents = X_train.shape[0]
if config.arch == 0: #Couzin Torus
    X_train, Y1_train, Y2_train = torus_reshape(X_train, Y1_train, Y2_train)
    X_val, Y1_val, Y2_val = torus_reshape(X_val, Y1_val, Y2_val)
elif config.arch == 1: # Moritz Ladder net
    valbatchsize = 16
    X_train, Y1_train, Y2_train = conv_lstm_reshape(X_train, Y1_train, Y2_train, config.stride, valbatchsize=16, stateful=config.stateful, train=True)
    X_val, Y1_val, Y2_val = conv_lstm_reshape(X_val, Y1_val, Y2_val, config.stride, valbatchsize=16, stateful=config.stateful)
    if config.stateful: # make Val-batches compatible with fixed batch size for config.stateful prediction
        X_val = X_val[:, :X_val.shape[1] - X_val.shape[1] % valbatchsize]
        Y1_val = Y1_val[:, :X_val.shape[1] - X_val.shape[1] % valbatchsize]
        Y2_val = Y2_val[:, :X_val.shape[1] - X_val.shape[1] % valbatchsize]
else: #LadderNet
    valbatchsize=16
    if config.custom_loop:
        X_train, Y1_train, Y2_train = ladder_Net_reshape_custom_loop(X_train, Y1_train, Y2_train, stride=config.stride, valbatchsize=valbatchsize, train=True)
        X_val, Y1_val, Y2_val = ladder_Net_reshape_custom_loop(X_val, Y1_val, Y2_val, config.stride, valbatchsize=valbatchsize, train=False)
    else:
        X_train, Y1_train, Y2_train = ladder_Net_reshape(X_train, Y1_train, Y2_train, stride=config.stride, stateful=config.stateful)
        X_val, Y1_val, Y2_val = ladder_Net_reshape(X_val, Y1_val, Y2_val, stride=config.stride, stateful=config.stateful)
        if config.stateful: # make Val-batches compatible with fixed batch size for config.stateful prediction
            X_val = X_val[0:X_val.shape[0] - X_val.shape[0] % num_agents]
            Y1_val = Y1_val[0:Y1_val.shape[0] - Y1_val.shape[0] % num_agents]
            Y2_val = Y2_val[0:Y2_val.shape[0] - Y2_val.shape[0] % num_agents]

#        ------------------build model---------------------------------
batchsize = num_agents # 96, if all of the data is used. Must be exactly num agents for stateful prediction without custom loop
if config.arch == 0:
    model = Moritz_Couzin_Tourus((config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.speed_bins, config.ang_vel_bins)
elif config.arch == 1:
    model = Moritz_Ladder_Network_Live_Female((valbatchsize, config.model_time_steps, 1, 2, config.no_of_bins_voa + 1), config.speed_bins, config.ang_vel_bins, config.stateful)
elif config.arch == 2 and not config.mixture_density:
    if config.custom_loop:
        model = LSTM_simple((valbatchsize, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.speed_bins, config.ang_vel_bins, config.stateful)
    else:
        model = Ladder_Net((batchsize, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.speed_bins, config.ang_vel_bins, config.stateful)
        #model = LSTM_simple((num_agents, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.speed_bins, config.ang_vel_bins, config.stateful)
        #model = Ladder_Conv_Net((num_agents, config.model_time_steps, 1, 2, config.no_of_bins_voa + 1), config.speed_bins, config.ang_vel_bins)
else:
    if not config.custom_loop:
        model = Ladder_Net_MD((batchsize, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.mixture_components, config.stateful)
        #model = Ladder_Net_MD_simple((batchsize, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.mixture_components, config.stateful)
    else:
        model = Ladder_Net_MD((valbatchsize, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.mixture_components, config.stateful)
        #model = Ladder_Net_MD_simple((valbatchsize, config.model_time_steps, config.no_of_bins_voa + config.no_of_bins_vow + 2), config.mixture_components, config.stateful)


print_shapes((X_train, Y1_train, Y2_train, X_val, Y1_val, Y2_val),
             ("X_train", "Y1_train", "Y2_train", "X_val", "Y1_val", "Y2_val"))

# --------------------Training-------------------------
if config.arch != 1 and not config.custom_loop:
    history_train = model.fit([X_train], [Y1_train, Y2_train], initial_epoch=config.starting_time_step, epochs=config.training_epochs,
                              shuffle=not config.stateful, validation_data=([X_val], [Y1_val, Y2_val]), batch_size=batchsize, verbose=1,
                              callbacks=[tensorboard_callback, ResetHidden(), SaveModel()])
else: # custom for loop to reset the states
    validation_history = {'loss': [], 'config.speed_loss': [], 'config.ang_vel_loss': [], 'config.speed_acc': [], 'config.ang_vel_acc': []}
    train_history = {'loss': [], 'config.speed_loss': [], 'config.ang_vel_loss': [], 'config.speed_acc': [], 'config.ang_vel_acc': []}
    log_dir = config.model_path + "tblogs"
    summary_writer = tf.summary.create_file_writer(log_dir)

    for i in range(config.starting_time_step, config.training_epochs):
        print("Iteration Number:", i + 1)
        print("Training:")
        for m in range(X_train.shape[0]):
            print("batch", m)
            overall_loss = 0
            config.speed_loss = 0
            ang_loss = 0
            config.speed_acc = 0
            ang_acc = 0

            for l in range(X_train.shape[1]):
                # history_train = model.fit(X_train[m][l], [Y1_train[m][l], Y2_train[m][l]], verbose=1, shuffle=False, batch_size=valbatchsize, callbacks=[tensorboard_callback])
                history_train = model.train_on_batch(X_train[m][l], [Y1_train[m][l], Y2_train[m][l]])
                overall_loss += history_train[0]
                config.speed_loss += history_train[1]
                ang_loss += history_train[2]
                config.speed_acc += history_train[3]
                ang_acc += history_train[4]

            overall_loss /= X_train.shape[1]
            config.speed_loss /= X_train.shape[1]
            ang_loss /= X_train.shape[1]
            config.speed_acc /= X_train.shape[1]
            ang_acc /= X_train.shape[1]

            print(overall_loss, " ", config.speed_loss, " ", ang_loss, " ", config.speed_acc, " ", ang_acc)


            with summary_writer.as_default():
                tf.summary.histogram("hidden activation", model.get_layer("lstm").states[0], step= i * X_train.shape[0] + m)
                tf.summary.histogram("cell activation", model.get_layer("lstm").states[1], step= i * X_train.shape[0] + m)


            print("resetting hidden states")
            model.get_layer("lstm").reset_states()

        print("Validation:")
        for m in range(X_val.shape[0]):
            history_validation = model.evaluate(X_val[m], [Y1_val[m], Y2_val[m]], batch_size=valbatchsize)

        model.save(config.model_path)


