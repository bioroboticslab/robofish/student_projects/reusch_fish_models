import config
import numpy as np

# adapted from https://towardsdatascience.com/fast-and-robust-sliding-window-vectorization-with-numpy-3ad950ed62f5
def sliding_window(array, window_size, stride):
    """takes an at least 2-dimensional array and slices the second dimension in slices of windowsize,
    overlapping after stride steps. if stride == window_size, the windows are non-overlapping"""
    sub_sequences = np.expand_dims(np.arange(window_size), 0) + \
                    np.expand_dims(np.arange(0, array.shape[1] - window_size, stride), 0).T
    return array[:, sub_sequences]



def torus_reshape(X, Y1, Y2):
    X_shape, Y1_shape, Y2_shape = np.shape(X), np.shape(Y1), np.shape(Y2)
    X = np.reshape(X[:, 0: (X_shape[1] - X_shape[1] % config.config.model_time_steps), :], (
        X_shape[0] * int((X_shape[1] - X_shape[1] % config.model_time_steps) / config.model_time_steps),
        config.model_time_steps, X_shape[2]))
    Y1 = np.reshape(Y1[:, 0: (Y1_shape[1] - Y1_shape[1] % config.model_time_steps), :], (
        Y1_shape[0] * int((Y1_shape[1] - Y1_shape[1] % config.model_time_steps) / config.model_time_steps),
        config.model_time_steps, Y1_shape[2]))
    Y2 = np.reshape(Y2[:, 0: (Y2_shape[1] - Y2_shape[1] % config.model_time_steps), :], (
        Y2_shape[0] * int((Y2_shape[1] - Y2_shape[1] % config.model_time_steps) / config.model_time_steps),
        config.model_time_steps, Y2_shape[2]))

    return X, Y1, Y2

def ladder_Net_reshape(X, Y1, Y2, stride, stateful=False, conv_layer=False):
    # input of dim (num_agents, tracklength, voa + vow + 2)
    if conv_layer:
        X = X.reshape(X.shape[0], X.shape[1], 1, 2, config.no_of_bins_voa + 1)
    if stateful: # shuffle data
        p = np.random.permutation(X.shape[0])
        X, Y1, Y2= X[p], Y1[p], Y2[p]

    # divide the overall sequence into smaller subsequences. for stride = 0:
    # (num_agents, tracklength / timesteps, timesteps, voa + vow + 2)
    X = sliding_window(X, config.model_time_steps, stride=stride)
    Y1 = sliding_window(Y1, config.model_time_steps, stride=stride)
    Y2 = sliding_window(Y2, config.model_time_steps, stride=stride)
    if stateful: # swap first two dimensions so that example i in batch j is the followup sequence of example i in batch j - 1
        X = X.transpose([1, 0] + list(range(2, len(X.shape))))
        Y1 = Y1.transpose([1, 0, 2, 3])
        Y2 = Y2.transpose([1, 0, 2, 3])

    # collapse first two dimensions again, if we now call with batchsize num agents, we have the right ordering of sequences per batch
    # for stateful prediction.
    X = X.reshape((-1, *X.shape[2:]))
    Y1 = Y1.reshape((-1, *Y1.shape[2:]))
    Y2 = Y2.reshape((-1, *Y2.shape[2:]))

    return X, Y1, Y2

def ladder_Net_reshape_custom_loop(X, Y1, Y2, stride, train, valbatchsize, conv_layer=False):
    if conv_layer:
        X = X.reshape(X.shape[0], X.shape[1], 1, 2, config.no_of_bins_voa + 1)
    X = sliding_window(X, config.model_time_steps, stride=stride)
    Y1= sliding_window(Y1, config.model_time_steps, stride=stride)
    Y2= sliding_window(Y2, config.model_time_steps, stride=stride)

    X = X.transpose([1, 0, 2, 3])
    Y1 = Y1.transpose([1, 0, 2, 3])
    Y2 = Y2.transpose([1, 0, 2, 3])

    # split the batches into smaller subbatches so that we dont have to feed batches of size num_files x config.model_time_steps
    # to the network, which is too large for memory
    if train:
        numfilesmodbatch = X.shape[1] - X.shape[1] % valbatchsize
        X = X[:, :numfilesmodbatch].reshape((X.shape[0], numfilesmodbatch // valbatchsize, valbatchsize, *X.shape[2:]))
        Y1 = Y1[:, :numfilesmodbatch].reshape((Y1.shape[0], numfilesmodbatch // valbatchsize, valbatchsize, *Y1.shape[2:]))
        Y2 = Y2[:, :numfilesmodbatch].reshape((Y2.shape[0], numfilesmodbatch // valbatchsize, valbatchsize, *Y2.shape[2:]))
        X = X.transpose([1, 0, 2, 3, 4])
        Y1 = Y1.transpose([1, 0, 2, 3, 4])
        Y2 = Y2.transpose([1, 0, 2, 3, 4])
    else:
        X = X[:, :X.shape[1] - X.shape[1] % valbatchsize]
        Y1 = Y1[:, :X.shape[1] - X.shape[1] % valbatchsize]
        Y2 = Y2[:, :X.shape[1] - X.shape[1] % valbatchsize]

    return X, Y1, Y2


def conv_lstm_reshape(X, Y1, Y2, stride, valbatchsize, stateful=False, train=False):
    # input of dim (num_agents, tracklength, embedding_dim)
    # match the expected dimension of conv net
    X = X.reshape(X.shape[0], X.shape[1], 1, 2, config.no_of_bins_voa + 1)
    # divide the 10000 timesteps into smaller timesteps -> (num_agents, tracklength / timesteps, timesteps, embedding_dim)
    X = sliding_window(X, config.model_time_steps, stride=stride)
    Y1 = sliding_window(Y1, config.model_time_steps, stride=stride)
    Y2 = sliding_window(Y2, config.model_time_steps, stride=stride)
    if stateful: # swap first two dimensions so that example i in batch j is the followup sequence of example i in batch j - 1
        X = X.transpose([1, 0, 2, 3, 4, 5])
        Y1 = Y1.transpose([1, 0, 2, 3])
        Y2 = Y2.transpose([1, 0, 2, 3])

    if train:
        numfilesmodbatch = X.shape[1] - X.shape[1] % valbatchsize
        X = X[:, :numfilesmodbatch].reshape((X.shape[0], numfilesmodbatch // valbatchsize, valbatchsize, *X.shape[2:]))
        Y1 = Y1[:, :numfilesmodbatch].reshape((Y1.shape[0], numfilesmodbatch // valbatchsize, valbatchsize, *Y1.shape[2:]))
        Y2 = Y2[:, :numfilesmodbatch].reshape((Y2.shape[0], numfilesmodbatch // valbatchsize, valbatchsize, *Y2.shape[2:]))
        X = X.transpose([1, 0, 2, 3, 4, 5, 6])
        Y1 = Y1.transpose([1, 0, 2, 3, 4])
        Y2 = Y2.transpose([1, 0, 2, 3, 4])
    return X, Y1, Y2


def print_shapes(arrays, headings):
    for a, h in zip(arrays, headings):
        print(h + ": " + str(a.shape))
    print()

