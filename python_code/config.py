import numpy as np
import glob

#model architecture
arch = 2  # 0 for Couzin Torus, 1 for ConvLSTM, 2 for LadderNet
stateful = True
custom_loop = False
mixture_density = True

# paths to data
# -------- change to your local path ----------------
couzin_torus_path = "/home/david/PycharmProjects/GuppySwarmBehaviour/guppy_data/couzin_torus/"
live_female_path = "/home/david/PycharmProjects/GuppySwarmBehaviour/guppy_data/live_female_female/"
model_path = '/home/david/swp/reusch_fish-models/Saved_Models/'
# ------------------------------------------------------------

model_path += "mixture_density" if mixture_density else "multimodal"
model_path += "_stateful" if stateful else ""
model_path += "1.2/"

train_path_couzin_torus = glob.glob(couzin_torus_path + "train/*.hdf5")
test_path_couzin_torus = glob.glob(couzin_torus_path + "test/*.hdf5")
validation_path_couzin_torus = glob.glob(couzin_torus_path + "validation/*.hdf5")

train_path_live_female_female = glob.glob(live_female_path + "train/*.hdf5")
test_path_live_female_female = glob.glob(live_female_path + "test/*.hdf5")
validation_path_live_female_female = glob.glob(live_female_path + "validation/*.hdf5")

validation_path_live_female_female = validation_path_live_female_female + test_path_live_female_female

if arch == 0:
    train_path = train_path_couzin_torus
    test_path = test_path_couzin_torus
    validation_path = validation_path_couzin_torus
else:
    train_path = train_path_live_female_female
    test_path = test_path_live_female_female
    validation_path = validation_path_live_female_female



# input and output format
if arch == 0:
    max_dist = np.sqrt(100 * 100 * 2)
    no_of_bins_voa = 21
    total_angle_voa = np.pi * 300 / 180
    no_of_bins_vow = 15
    total_angle_vow = np.pi
    wall_boundaries = np.array([50, -50, 50, -50])
    max_speed = 0.32
    speed_bins = 41
    max_ang_velo = 0.75
    ang_vel_bins = 217
else:
    max_dist = np.sqrt(100 * 100 * 2)
    no_of_bins_voa = 151
    total_angle_voa = np.pi * 300 / 180
    no_of_bins_vow = 151
    total_angle_vow = np.pi * 298 / 180
    wall_boundaries = np.array([50, -50, 50, -50])
    max_speed = 0.8
    speed_bins = 215 # 90
    max_ang_velo = 0.8
    ang_vel_bins = 215 # 90

# Training Hyperparameters
training_epochs = 100
model_time_steps = 75
stride = model_time_steps
starting_time_step = 0
mixture_components = 20

