import tensorflow.keras as keras
import tensorflow as tf
import tensorflow.keras.backend as K
import numpy as np

import config


class ResetHidden(keras.callbacks.Callback):
    def on_epoch_begin(self, epoch, logs=None):
        if self.model.stateful:
            print("resetting hidden states")
            self.model.reset_states()

    def on_epoch_end(self, epoch, logs=None):
        log_dir = config.model_path + "tblogs"
        summary_writer = tf.summary.create_file_writer(log_dir)

        if self.model.stateful:
            # add the distribution of hidden state and cell state of first layer to tensorboard
            with summary_writer.as_default():
                tf.summary.histogram("hidden activation 1. layer", self.model.get_layer("lstm").states[0], step=epoch)
                tf.summary.histogram("cell activation 1. layer", self.model.get_layer("lstm").states[1], step=epoch)
            print("resetting hidden states")
            self.model.reset_states()


class SaveModel(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        if epoch % 5 == 0:
            if not config.mixture_density:
                self.model.save(config.model_path + "model.h5")
            self.model.save_weights(config.model_path + "weights.h5", overwrite=True)


tensorboard_callback = keras.callbacks.TensorBoard(log_dir=config.model_path + "tblogs", histogram_freq=1, write_images=True,
                                                   write_grads=True)


