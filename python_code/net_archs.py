from tensorflow.keras import layers, models, optimizers
from tensorflow.keras import losses
import tensorflow.keras as keras
import tensorflow.keras.backend as K
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
import sys
import config

tfd = tfp.distributions


# see moritz master thesis
# convLSTM|layernorm -> LSTM|layernorm -> LSTM|layernorm -> 2 * FC Output
def Moritz_Ladder_Network_Live_Female(shape_, output_bins_speed, output_bins_av, stateful):
    input_ = layers.Input(batch_shape=shape_) if stateful else layers.Input(shape=shape_[1:])
    convLSTM_1 = layers.ConvLSTM2D(filters=8, kernel_size=(9, 9), data_format='channels_first', padding='same',
                                   return_sequences=True, stateful=stateful, name="conv_lstm")(input_)
    norm_1 = layers.LayerNormalization()(convLSTM_1)
    flatten = layers.TimeDistributed(layers.Flatten())(norm_1)
    norm_2 = layers.LayerNormalization()(flatten)
    h1 = layers.LSTM(100, return_sequences=True, stateful=stateful, name="lstm1")(norm_2)
    norm_3 = layers.LayerNormalization()(h1)
    h2 = layers.LSTM(100, return_sequences=True, stateful=stateful, name="lstm2")(norm_3)
    norm_4 = layers.LayerNormalization()(h2)
    norm_5 = norm_4
    speed_binned = layers.TimeDistributed(layers.Dense(output_bins_speed, activation='softmax', name='speed_binned'))(
        norm_5)
    angular_velocity_binned = layers.TimeDistributed(
        layers.Dense(output_bins_av, activation='softmax', name='angular_velocity_binned'))(norm_5)
    opt = optimizers.Adam(lr=0.0002)
    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned], name='moritzs_ladder_network')
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy], optimizer=opt,
                  metrics=['categorical_accuracy'])

    return model


# keras top k accuracy doesnt account for series data, so we have to expand first dimension before calling the function
def my_top_k(true, pred):
    true = K.reshape(true, (-1, config.ang_vel_bins))
    pred = K.reshape(pred, (-1, config.ang_vel_bins))
    return keras.metrics.top_k_categorical_accuracy(true, pred, k=5)


def index_to_value(i, min, max, num_bins):
    print(i)
    i = tf.cast(i, tf.float32)
    print(i)
    bin_width = (max - min) / num_bins
    start = i * bin_width + min
    end = start + bin_width
    return tf.random.uniform()


def mse_pred_loss(type):
    """not finished! let model predict a value and compare it to the true value, compute mse loss"""

    def mse_loss(true, pred):
        print(pred.shape)
        print(true.shape)
        pred = tf.reshape(pred, (-1, pred.shape[-1]))
        true = tf.reshape(true, (-1, true.shape[-1]))
        print(pred.shape)
        dist = tfd.Categorical(probs=pred)
        samples = index_to_value(dist.sample(), -mini, maxi, config.ang_vel_bins)
        print(samples.shape)
        true_indices = tf.argmax(true, axis=-1)
        print(true_indices.shape)
        true_values = index_to_value(true_indices, -config.max_ang_velo, config.max_ang_velo, num_bins)
        print(true_values.shape)
        losses = (samples - true_values) ** 2
        return tf.reduce_mean(losses)

    if type == "speed":
        mini = -config.max_speed
        maxi = config.max_speed
        num_bins = config.speed_bins
        return mse_loss
    else:
        mini = -config.max_ang_velo
        maxi = config.max_ang_velo
        num_bins = config.ang_vel_bins
        return mse_loss


def Linear_Model(shape_, output_bins_speed, output_bins_av):
    input_ = layers.Input(
        shape=shape_)  # layers.Input(batch_shape=shape_) if stateful else layers.Input(shape= shape_[1:])
    enc1 = (layers.Dense(300, activation="relu"))(input_)
    norm_enc1 = layers.LayerNormalization()(enc1)
    drp_enc1 = layers.Dropout(0.5)(norm_enc1)
    enc2 = (layers.Dense(300, activation="relu"))(drp_enc1)
    norm_enc2 = layers.LayerNormalization()(enc2)
    drp_enc2 = layers.Dropout(0.5)(norm_enc2)
    speed_binned = (layers.Dense(output_bins_speed, activation='softmax', name='speed_binned'))(
        drp_enc2)
    angular_velocity_binned = (
        layers.Dense(output_bins_av, activation='softmax', name='angular_velocity_binned'))(drp_enc2)
    opt = optimizers.Adam(lr=0.0001)
    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned], name='ladder_net')
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy], optimizer=opt,
                  metrics=["categorical_accuracy", my_top_k])
    return model


# lstm -> layernorm -> dropout -> 2 * FC Output
def LSTM_simple(shape_, output_bins_speed, output_bins_av, stateful=False):
    input_ = layers.Input(batch_shape=shape_) if stateful else layers.Input(shape=shape_[1:])
    # randuniinit = tf.keras.initializers.RandomUniform(minval=-5, maxval=5, seed=None)
    enc1 = layers.LSTM(200, return_sequences=True, stateful=stateful, name="lstm")(input_)
    # norm_enc1 = layers.LayerNormalization()(enc1)
    drp_enc1 = layers.Dropout(0.5)(enc1)
    speed_binned = layers.TimeDistributed(layers.Dense(output_bins_speed, activation='softmax', name='speed_binned'))(
        drp_enc1)
    angular_velocity_binned = layers.TimeDistributed(
        layers.Dense(output_bins_av, activation='softmax', name='angular_velocity_binned'))(drp_enc1)
    opt = optimizers.Adam(lr=0.0001)
    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned], name='ladder_net')
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy], optimizer=opt,
                  metrics=["categorical_accuracy", my_top_k])
    return model


# the ladder_net inspired by Eyolfsdottir, see the literature list in the repo
def Ladder_Net(shape_, output_bins_speed, output_bins_av, stateful=False):
    input_ = layers.Input(batch_shape=shape_) if stateful else layers.Input(shape=shape_[1:])

    enc1 = layers.LSTM(200, return_sequences=True, stateful=stateful)(input_)
    norm_enc1 = layers.LayerNormalization()(enc1)
    drp_enc1 = layers.Dropout(0.5)(norm_enc1)
    enc2 = layers.LSTM(200, return_sequences=True, stateful=stateful)(drp_enc1)
    norm_enc2 = layers.LayerNormalization()(enc2)
    drp_enc2 = layers.Dropout(0.5)(norm_enc2)
    enc3 = layers.LSTM(200, return_sequences=True, stateful=stateful)(drp_enc2)
    norm_enc3 = layers.LayerNormalization()(enc3)
    drp_enc3 = layers.Dropout(0.5)(norm_enc3)

    dec3 = layers.LSTM(200, return_sequences=True, stateful=stateful)(drp_enc3)
    norm_dec3 = layers.LayerNormalization()(dec3)
    drp_dec3 = layers.Dropout(0.5)(norm_dec3)
    comb_nenc2_ndec3 = layers.concatenate([drp_enc2, drp_dec3])
    dec2 = layers.LSTM(200, return_sequences=True, stateful=stateful)(comb_nenc2_ndec3)
    norm_dec2 = layers.LayerNormalization()(dec2)
    drp_dec2 = layers.Dropout(0.5)(norm_dec2)
    comb_nenc1_ndec2 = layers.concatenate([drp_enc1, drp_dec2])
    dec1 = layers.LSTM(200, return_sequences=True, stateful=stateful)(comb_nenc1_ndec2)
    norm_dec1 = layers.LayerNormalization()(dec1)

    speed_binned = layers.TimeDistributed(layers.Dense(output_bins_speed, activation='softmax'), name='speed_binned')(
        norm_dec1)
    angular_velocity_binned = layers.TimeDistributed(layers.Dense(output_bins_av, activation='softmax'),
                                                     name='config.ang_vel_binned')(norm_dec1)
    opt = optimizers.Adam(lr=0.001)
    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned], name='ladder_net')
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy], optimizer=opt,
                  metrics={'speed_binned': ("categorical_accuracy", my_top_k, mse_pred_loss("speed")),
                           'config.ang_vel_binned': ("categorical_accuracy", my_top_k, mse_pred_loss("angle"))})

    return model


def Ladder_Conv_Net(shape_, output_bins_speed, output_bins_av, stateful=False):
    # the LadderNet with a conv layer prepended

    input_ = layers.Input(batch_shape=shape_) if stateful else layers.Input(shape=shape_[1:])

    convLSTM_1 = layers.ConvLSTM2D(filters=8, kernel_size=(9, 9), data_format='channels_first', padding='same',
                                   return_sequences=True, stateful=stateful, name="conv_lstm")(input_)
    flatten = layers.TimeDistributed(layers.Flatten())(convLSTM_1)
    norm_flatten = layers.LayerNormalization()(flatten)
    enc1 = layers.LSTM(100, return_sequences=True, stateful=stateful)(norm_flatten)
    enc1 = layers.LayerNormalization()(enc1)
    norm_enc1 = layers.Dropout(0.5)(enc1)
    enc2 = layers.LSTM(100, return_sequences=True, stateful=stateful)(norm_enc1)
    norm_enc2 = layers.LayerNormalization()(enc2)
    norm_enc2 = layers.Dropout(0.5)(norm_enc2)
    enc3 = layers.LSTM(100, return_sequences=True, stateful=stateful)(norm_enc2)
    norm_enc3 = layers.LayerNormalization()(enc3)
    norm_enc3 = layers.Dropout(0.5)(norm_enc3)

    dec3 = layers.LSTM(100, return_sequences=True, stateful=stateful)(norm_enc3)
    norm_dec3 = layers.LayerNormalization()(dec3)
    norm_dec3 = layers.Dropout(0.5)(norm_dec3)
    comb_nenc2_ndec3 = layers.concatenate([norm_enc2, norm_dec3])
    dec2 = layers.LSTM(100, return_sequences=True, stateful=stateful)(comb_nenc2_ndec3)
    norm_dec2 = layers.LayerNormalization()(dec2)
    norm_dec2 = layers.Dropout(0.5)(norm_dec2)
    comb_nenc1_ndec2 = layers.concatenate([norm_enc1, norm_dec2])
    dec1 = layers.LSTM(100, return_sequences=True, stateful=stateful)(comb_nenc1_ndec2)
    norm_dec1 = layers.LayerNormalization()(dec1)
    norm_dec1 = layers.Dropout(0.5)(norm_dec1)

    speed_binned = layers.TimeDistributed(layers.Dense(output_bins_speed, activation='softmax', name='speed_binned'))(
        norm_dec1)
    angular_velocity_binned = layers.TimeDistributed(
        layers.Dense(output_bins_av, activation='softmax', name='angular_velocity_binned'))(norm_dec1)
    opt = optimizers.Adam(lr=0.0001)
    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned], name='ladder_net')
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy], optimizer=opt,
                  metrics=["categorical_accuracy", my_top_k])

    return model


# ----------------------Gaussian Mixture Density Model ----------------------------------


def nnelu(input):
    """ Computes the Non-Negative Exponential Linear Unit
    """
    return tf.add(tf.constant(1, dtype=tf.float32), tf.nn.elu(input))


tf.keras.utils.get_custom_objects().update({'nnelu': keras.layers.Activation(nnelu)})


# define custom loss
def md_loss(y_true, y_pred):
    """ Computes the mean negative log-likelihood loss of y given the mixture parameters.
    """
    y_pred = tf.reshape(y_pred, (-1, y_pred.shape[-1]))
    y_true = tf.reshape(y_true, (-1, y_true.shape[-1]))
    alpha, mu, sigma = [y_pred[:, i * config.mixture_components:(i + 1) * config.mixture_components] for i in
                        range(3)]  # Unpack parameter vectors
    gm = tfd.MixtureSameFamily(
        mixture_distribution=tfd.Categorical(probs=alpha),
        components_distribution=tfd.Normal(
            loc=mu,
            scale=sigma))

    log_likelihood = gm.log_prob(tf.transpose(y_true))  # Evaluate log-probability of y
    loss = -tf.reduce_mean(log_likelihood, axis=-1)
    return loss  # -tf.reduce_mean(log_likelihood, axis=-1)


# define custom metrics
def md_confidence(y_true, y_pred):
    y_pred = tf.reshape(y_pred, (-1, y_pred.shape[-1]))
    y_true = tf.reshape(y_true, (-1, y_true.shape[-1]))
    alpha, mu, sigma = [y_pred[:, i * config.mixture_components:(i + 1) * config.mixture_components] for i in
                        range(3)]  # Unpack parameter vectors
    gm = tfd.MixtureSameFamily(
        mixture_distribution=tfd.Categorical(probs=alpha),
        components_distribution=tfd.Normal(
            loc=mu,
            scale=sigma))
    probs = gm.prob(tf.transpose(y_true))  # Evaluate log-probability of y
    mean_acc = tf.reduce_mean(probs, axis=-1)
    return mean_acc  # -tf.reduce_mean(log_likelihood, axis=-1)


def mse_sample_loss(y_true, y_pred):
    y_pred = tf.reshape(y_pred, (-1, y_pred.shape[-1]))
    y_true = tf.reshape(y_true, (-1, y_true.shape[-1]))
    alpha, mu, sigma = [y_pred[:, i * config.mixture_components:(i + 1) * config.mixture_components] for i in
                        range(3)]  # Unpack parameter vectors
    gm = tfd.MixtureSameFamily(
        mixture_distribution=tfd.Categorical(probs=alpha),
        components_distribution=tfd.Normal(
            loc=mu,
            scale=sigma))
    samples = tf.reshape(gm.sample(), (-1, 1))
    losses = (samples - y_true) ** 2
    return tf.reduce_mean(losses)


# the same network as above, but output is represented as a mixture of gaussian distributions
# adapted from https://towardsdatascience.com/a-hitchhikers-guide-to-mixture-density-networks-76b435826cca
def Ladder_Net_MD(shape_, components, stateful=False):
    Drp = layers.Dropout(0.5)
    input_ = layers.Input(batch_shape=shape_) if stateful else layers.Input(shape=shape_[1:])

    # encoding path
    # if we set recurrent dropout, lstm cannot be trained on cpu
    enc1 = layers.LSTM(200, return_sequences=True, stateful=stateful)(input_)
    norm_enc1 = Drp(layers.BatchNormalization()(enc1))
    enc2 = layers.LSTM(200, return_sequences=True, stateful=stateful)(norm_enc1)
    norm_enc2 = Drp(layers.BatchNormalization()(enc2))
    enc3 = layers.LSTM(200, return_sequences=True, stateful=stateful)(norm_enc2)
    norm_enc3 = Drp(layers.BatchNormalization()(enc3))

    # decoding path
    dec3 = layers.LSTM(200, return_sequences=True, stateful=stateful)(norm_enc3)
    norm_dec3 = Drp(layers.BatchNormalization()(dec3))
    comb_nenc2_ndec3 = layers.concatenate([norm_enc2, norm_dec3])
    dec2 = layers.LSTM(200, return_sequences=True, stateful=stateful)(comb_nenc2_ndec3)
    norm_dec2 = Drp(layers.BatchNormalization()(dec2))
    comb_nenc1_ndec2 = layers.concatenate([norm_enc1, norm_dec2])
    dec1 = layers.LSTM(200, return_sequences=True, stateful=stateful)(comb_nenc1_ndec2)
    norm_dec1 = Drp(layers.LayerNormalization()(dec1))

    # output params with fc layers
    speed_alphas = layers.TimeDistributed(layers.Dense(components, activation="softmax", name="speed_alphas"))(
        norm_dec1)  # Create vector for alpha (softmax constrained)
    speed_mus = layers.TimeDistributed(layers.Dense(components, name="speed_mus"))(norm_dec1)  # Create vector for mus
    speed_sigmas = layers.TimeDistributed(layers.Dense(components, activation="nnelu", name="speed_sigmas"))(
        norm_dec1)  # Create vector sigmas (nnelu constrained)
    speed_params = layers.Concatenate(name="speed_params")(
        [speed_alphas, speed_mus, speed_sigmas])  # Concatenate (required for model compilation)

    angle_alphas = layers.TimeDistributed(layers.Dense(components, activation="softmax", name="speed_alphas"))(
        norm_dec1)  # Create vector for alpha (softmax constrained)
    angle_mus = layers.TimeDistributed(layers.Dense(components, name="speed_mus"))(norm_dec1)  # Create vector for mus
    angle_sigmas = layers.TimeDistributed(layers.Dense(components, activation="nnelu", name="speed_sigmas"))(
        norm_dec1)  # Create vector sigmas (nnelu constrained)
    angle_params = layers.Concatenate(name="angle_params")(
        [angle_alphas, angle_mus, angle_sigmas])  # Concatenate (required for model compilation)

    model = models.Model(inputs=input_, outputs=[speed_params, angle_params], name='ladder_net')
    opt = optimizers.Adam(lr=0.0001)
    model.compile(loss=[md_loss, md_loss], optimizer=opt, metrics=[md_confidence, mse_sample_loss])

    return model


# adapted from https://towardsdatascience.com/a-hitchhikers-guide-to-mixture-density-networks-76b435826cca
def Ladder_Net_MD_simple(shape_, components, stateful=False):
    input_ = layers.Input(batch_shape=shape_) if stateful else layers.Input(shape=shape_[1:])

    enc1 = layers.LSTM(200, return_sequences=True, stateful=stateful, name="lstm")(input_)
    norm_enc1 = layers.LayerNormalization()(enc1)

    speed_alphas = layers.TimeDistributed(layers.Dense(components, activation="softmax", name="speed_alphas"))(
        norm_enc1)  # Create vector for alpha (softmax constrained)
    speed_mus = layers.TimeDistributed(layers.Dense(components, name="speed_mus"))(norm_enc1)  # Create vector for mus
    speed_sigmas = layers.TimeDistributed(layers.Dense(components, activation="nnelu", name="speed_sigmas"))(
        norm_enc1)  # Create vector sigmas (nnelu constrained)
    speed_params = layers.Concatenate(name="speed_params")(
        [speed_alphas, speed_mus, speed_sigmas])  # Concatenate (required for model compilation)

    angle_alphas = layers.TimeDistributed(layers.Dense(components, activation="softmax", name="angle_alphas"))(
        norm_enc1)  # Create vector for alpha (softmax constrained)
    angle_mus = layers.TimeDistributed(layers.Dense(components, name="angle_mus"))(norm_enc1)  # Create vector for mus
    angle_sigmas = layers.TimeDistributed(layers.Dense(components, activation="nnelu", name="angle_sigmas"))(
        norm_enc1)  # Create vector sigmas (nnelu constrained)
    angle_params = layers.Concatenate(name="angle_params")(
        [angle_alphas, angle_mus, angle_sigmas])  # Concatenate (required for model compilation)

    model = models.Model(inputs=input_, outputs=[speed_params, angle_params], name='ladder_net')
    opt = optimizers.Adam(lr=0.0005)
    model.compile(loss=[md_loss, md_loss], optimizer=opt, metrics=[md_confidence, mse_sample_loss])

    return model


def Ladder_Net_With_Y_Labels(shape_, shape_2, output_bins_speed, output_bins_av):
    input_ = layers.Input(shape=shape_)

    enc1 = layers.LSTM(200, return_sequences=True)(input_)
    norm_enc1 = layers.LayerNormalization()(enc1)
    enc2 = layers.LSTM(200, return_sequences=True)(norm_enc1)
    norm_enc2 = layers.LayerNormalization()(enc2)
    enc3 = layers.LSTM(200, return_sequences=True)(norm_enc2)
    norm_enc3 = layers.LayerNormalization()(enc3)

    dec3 = layers.LSTM(200, return_sequences=True)(norm_enc3)
    norm_dec3 = layers.LayerNormalization()(dec3)
    comb_nenc2_ndec3 = layers.concatenate([norm_enc2, norm_dec3])
    dec2 = layers.LSTM(200, return_sequences=True)(comb_nenc2_ndec3)
    norm_dec2 = layers.LayerNormalization()(dec2)
    comb_nenc1_ndec2 = layers.concatenate([norm_enc1, norm_dec2])
    dec1 = layers.LSTM(200, return_sequences=True)(comb_nenc2_ndec3)
    norm_dec1 = layers.LayerNormalization()(dec1)

    y_label_output = layers.TimeDistributed(layers.Dense(1, activation='tanh', name='y_label'))(norm_enc3)
    speed_binned = layers.TimeDistributed(layers.Dense(output_bins_speed, activation='softmax', name='speed_binned'))(
        norm_dec1)
    angular_velocity_binned = layers.TimeDistributed(
        layers.Dense(output_bins_av, activation='softmax', name='angular_velocity_binned'))(norm_dec1)
    opt = optimizers.Adam(lr=0.001)

    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned, y_label_output],
                         name='ladder_net_with_y_labels')
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy, losses.MSE], optimizer=opt,
                  metrics=['categorical_accuracy', 'accuracy'])

    return model


def Moritz_Couzin_Tourus(shape_, output_bins_speed, output_bins_av):
    input_ = layers.Input(shape=shape_)
    h1 = layers.LSTM(10, return_sequences=True)(input_)
    norm_1 = layers.LayerNormalization()(h1)
    speed_binned = layers.TimeDistributed(layers.Dense(output_bins_speed, activation='softmax', name='speed_binned'))(
        norm_1)
    angular_velocity_binned = layers.TimeDistributed(
        layers.Dense(output_bins_av, activation='softmax', name='angular_velocity_binned'))(norm_1)
    model = models.Model(inputs=input_, outputs=[speed_binned, angular_velocity_binned], name='moritz_couzin_tourus')
    opt = optimizers.Adam(learning_rate=0.0001)
    model.compile(loss=[losses.categorical_crossentropy, losses.categorical_crossentropy], optimizer=opt,
                  metrics=['categorical_accuracy'])

    return model
